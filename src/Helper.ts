export class WinCheck {
  
  // Todo: make for any size field. Work just for field 3x3
  public static Result(data: number[][], value: number) {

    var diagonals = this.checkDiagonals(data, value);
    var verticals = this.checkVerticals(data, value);
    var horizontals = this.checkHorizontals(data, value);

    if (diagonals) {
      return true;
    }

    if (verticals) {
      return true;
    }

    if (horizontals) {
      return true;
    }
  }

  static checkDiagonals = (state: number[][], val: number) => {
    if ((state[0][0] === val && state[1][1] === val && state[2][2] === val) ||
      (state[0][2] === val && state[1][1] === val && state[2][0] === val)) {
      return true;
    }
    return false;
  }

  static checkVerticals = (state: number[][], val: number) => {
    for (let i = 0; i <= 2; i++) {
      if (state[0][i] === val && state[1][i] === val && state[2][i] === val) {
        return true;
      }
    }
    return false;
  }

  static checkHorizontals = (state: number[][], val: number) => {
    for (let i = 0; i <= 2; i++) {
      if (state[i][0] === val && state[i][1] === val && state[i][2] === val) {
        return true;
      }
    }
    return false;
  }

}


// Check if any free cell left
export class EndCheck {
  public static Result(data: number[][]) {
    return !data.some(row => row.includes(0));
  }
}


export class GameLabelsConstants{
  public static readonly Welcome = "Welcome";
  public static readonly Restart = "Restart";
  public static readonly PlayerXTurn = "Player X turn";
  public static readonly PlayerOTurn = "Player O turn";
  public static readonly PlayerXWon = "Player X won!";
  public static readonly PlayerOWon = "Player O won!";
  public static readonly Draw = "Draw";
}
