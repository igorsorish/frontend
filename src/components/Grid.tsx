import React from 'react';
import styled from "styled-components";

import {Cell, CellProps} from './Cell';
import {StatusHeader} from './StatusHeader';
import {HistoryNavigator, HistoryType} from './HistoryNavigator';

import {WinCheck, EndCheck, GameLabelsConstants} from '../Helper';

  type GridState = {
    Data: number[][],
    CurrentType: number,
    CurrentStep: number,
    GameLabel: string,
    GameOver: boolean,
    History: HistoryType[]
  }

  type GridProps = {
    Size: number
  }
 
  const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(${(props:GridProps) => props.Size}, 70px);
    grid-template-rows: 70px repeat(${(props:GridProps) => props.Size}, 70px) 0;
    grid-auto-flow: row;
  `;

  const RestartButton = styled.button`
      color: #fff;
      background: none;
      border-color: #fff;
      border-style: solid;
      font-size: 2rem;
      cursor: pointer;
  `;

export class Grid extends React.Component<GridProps, GridState> {
    
    // Default state
    get initialState(){
      return {
        Data: Array(this.props.Size).fill(null).map(()=>Array(this.props.Size).fill(0)),
        CurrentType: 1,
        CurrentStep: 0,
        GameLabel: GameLabelsConstants.Welcome,
        GameOver: false,
        History: []
      }
    }

    constructor (props: GridProps) {
      super(props);
      this.state = this.initialState;
      this.onCellClick = this.onCellClick.bind(this);
      this.onHistoryClick = this.onHistoryClick.bind(this);
      this.onRestart = this.onRestart.bind(this);
    }

    onCellClick(props: CellProps){
      // If cell empty
      if (props.type == 0 && ! this.state.GameOver){
        let data = this.state.Data;
        let type = this.state.CurrentType;
        let step = this.state.CurrentStep;

        // Set new value
        data[props.xPos][props.yPos] = type;

        // Check if win
        let isWin = WinCheck.Result(data, type);

        // Check if game over
        let isEnd = EndCheck.Result(data);

        this.UpdateHistory(step, type, props.xPos, props.yPos);

        // Setup statge for next turn
        this.setState({
          Data: data,
          CurrentType: (type==1) ? 2 : 1,
          CurrentStep: ++step,
          GameLabel: (type==1) ? GameLabelsConstants.PlayerOTurn : GameLabelsConstants.PlayerXTurn
        });

        if (isWin){
          this.onGameWin(type);
        }

        if (isEnd && !isWin){
          this.onGameDraw();
        }

      }
    }

    UpdateHistory(step: number, type: number, xPos: number, yPos: number){
      let historyStep = {Type: type, Step: step, X: xPos, Y: yPos} as HistoryType;
      // Append all prev history
      let history: HistoryType[] = this.state.History;
      // Add new step
      history.push(historyStep);
      
      this.setState({
        History: history
      });
    }

    onHistoryClick(toStep: number){
      let history: HistoryType[] = this.state.History;
      if (history.length > toStep+1){
        //Get empty data
        let recoveredData: number[][] = Array(this.props.Size).fill(null).map(()=>Array(this.props.Size).fill(0));
        let type = (history[toStep].Type==1) ? 2 : 1;
        let step = history[toStep].Step;

        // Go data back step by step
        history = history.slice(0, toStep + 1);

        for (var i = 0; i < history.length; i++){
          recoveredData[history[i].X][history[i].Y] = history[i].Type;
        }

        this.setState({
          Data: recoveredData,
          GameLabel: ( type == 1) ? GameLabelsConstants.PlayerOTurn : GameLabelsConstants.PlayerXTurn,
          CurrentType: type,
          CurrentStep: ++step,
          GameOver: false,
          History: history
        });

      }

    }

    onGameWin(player: number){
      this.setState({
        GameOver: true,
        GameLabel: (player == 1) ? GameLabelsConstants.PlayerXWon : GameLabelsConstants.PlayerOWon
      });
    }

    onGameDraw(){
      this.setState({
        GameOver: true,
        GameLabel: GameLabelsConstants.Draw
      });
    }

    onRestart(){
      this.setState({
        ...this.initialState,
        GameLabel: GameLabelsConstants.Welcome
      });
    }

    renderCells(){
        let size = this.props.Size;
        let cells = [];
        let index = 0;
        for (var i=0; i < size; i++){
            for (var j = 0; j < size; j++){
                cells.push(<Cell key={index} type={this.state.Data[i][j]} xPos={i} yPos={j} onClick={this.onCellClick}/>);
                index++;
            }
        }
        return cells;
    }

    render() {
      return (
        <div>
          <StatusHeader>{this.state.GameLabel}</StatusHeader>
          <GridContainer Size={this.props.Size} className='tic-tac-toe-field'>
            {this.renderCells()}
          </GridContainer>
          <HistoryNavigator Data={this.state.History} OnHistoryBack={this.onHistoryClick}/>
          <RestartButton onClick={this.onRestart} hidden={!this.state.GameOver}>{GameLabelsConstants.Restart}</RestartButton>
        </div>
      );
    }
  }