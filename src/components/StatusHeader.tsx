import React from 'react';

interface StatusHeaderProps{
  children?: any
}

export class StatusHeader extends React.Component<StatusHeaderProps, {}>{

    render() {
      return (
        <h2>{this.props.children}</h2>
      );
    }
  }