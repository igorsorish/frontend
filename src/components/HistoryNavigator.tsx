import React from 'react';

interface HistoryNavigatorProps{
  Data: HistoryType[],
  OnHistoryBack: any
}

export type HistoryType = {
  Step: number,
  Type: number,
  X: number,
  Y: number
}

export class HistoryNavigator extends React.Component<HistoryNavigatorProps, {}>{

    onClick(step: number){
      this.props.OnHistoryBack(step);
    }

    render() {
      var data = this.props?.Data??[];
      return (
        <div>
          <h3>History navigation</h3>
        <ul> 
         {data.map((histroyItem)=>
               { 
                 return (
                 <li>
                   <a href="#" onClick={() => this.onClick(histroyItem.Step)} ><span className="date">{histroyItem.Step+1}</span> Player {histroyItem.Type==1?"X":"O"} [{histroyItem.X}, {histroyItem.Y}]</a>
                  </li>);
                })}
                
        </ul>
        </div>
      );
    }
  }