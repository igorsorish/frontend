import React from 'react';
import styled from "styled-components";

export interface CellProps {
  type: number,
  xPos: number,
  yPos: number,
  onClick: any,
}

// interface CellState {
//   type: number
// }

const CellContainer = styled.div`
      display: flex;
      align-items: center;
      justify-content: center;
      border: 1px solid white;
      text-align: center;
      &:hover {
        background: #171b23;
        cursor: pointer;
      }
  `;


export class Cell extends React.Component<CellProps, {}>{

    render() {
      const type = this.props.type;
      let symbol = "";
      switch(type){
        case 1: symbol = "X"; break;
        case 2: symbol = "O"; break 
      }
      
      return (
          <CellContainer onClick={() => this.props.onClick(this.props)}>
              {symbol}
          </CellContainer>
      );
    }
  }